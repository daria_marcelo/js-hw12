// 1) Тому що введення даних не обов'язково може здійснюватися тільки за допомогою клавіатури


const buttons = document.querySelectorAll('.btn')

document.addEventListener('keydown',(event)=>{
    buttons.forEach(item=>{
        item.getAttribute('data-btn') === event.code ? item.classList.add('active') : item.classList.remove('active')

    })
})
